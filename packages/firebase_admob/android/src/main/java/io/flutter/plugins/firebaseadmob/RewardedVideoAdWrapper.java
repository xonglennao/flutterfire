// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package io.flutter.plugins.firebaseadmob;

import android.app.Activity;
import android.util.Log;
import android.util.SparseArray;

import androidx.annotation.NonNull;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdValue;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.OnPaidEventListener;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.ResponseInfo;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.gms.ads.rewarded.ServerSideVerificationOptions;

import io.flutter.plugin.common.MethodChannel;
import java.util.HashMap;
import java.util.Map;

public class RewardedVideoAdWrapper {
  private static final String TAG = "flutter";

  private static SparseArray<RewardedVideoAdWrapper> allAds = new SparseArray<>();

  private RewardedAd rewardedInstance;
  private final MethodChannel channel;
  private Status status;
  private Activity activity;
  private RewardedAdLoadCallback rewardedAdLoadCallback;
  private FullScreenContentCallback fullScreenContentCallback;
  private OnPaidEventListener onPaidEventListener;
  private OnUserEarnedRewardListener onUserEarnedRewardListener;
  private String userId;
  private String customData;
  private int id;
  private ResponseInfo adResponseInfo;

  enum Status {
    CREATED,
    LOADING,
    FAILED,
    LOADED
  }

  private RewardedVideoAdWrapper(int id, Activity activity, MethodChannel channel) {
    this.id = id;
    this.activity = activity;
    this.channel = channel;
    this.status = Status.CREATED;
    allAds.put(id, this);
    this.rewardedAdLoadCallback = new RewardedAdLoadCallback() {

      @Override
      public void onAdLoaded(RewardedAd ad) {
        status = Status.LOADED;
        rewardedInstance = ad;
        adResponseInfo = rewardedInstance.getResponseInfo();
        buildServerSideVerificationOptions(userId, customData);
        rewardedInstance.setOnPaidEventListener(onPaidEventListener);
        rewardedInstance.setFullScreenContentCallback(fullScreenContentCallback);
        RewardedVideoAdWrapper.this.channel.invokeMethod("onRewardedAdLoaded", argumentsMap());
      }

      @Override
      public void onAdFailedToLoad(LoadAdError error) {
        Log.w(TAG, "onRewardedAdFailedToLoad: " + error);
        status = Status.FAILED;
        rewardedInstance = null;
        RewardedVideoAdWrapper.this.channel.invokeMethod("onRewardedAdFailedToLoad", argumentsMap("errorCode", error.getCode()));
      }
    };
    this.fullScreenContentCallback = new FullScreenContentCallback() {

      @Override
      public void onAdShowedFullScreenContent() {
        RewardedVideoAdWrapper.this.channel.invokeMethod("onRewardedAdOpened", argumentsMap());
      }

      @Override
      public void onAdDismissedFullScreenContent() {
        status = Status.CREATED;
        RewardedVideoAdWrapper.this.channel.invokeMethod("onRewardedAdClosed", argumentsMap());
      }

      @Override
      public void onAdFailedToShowFullScreenContent(AdError error) {
        Log.w(TAG, "onRewardedAdFailedToLoad: " + error);
        RewardedVideoAdWrapper.this.channel.invokeMethod("onRewardedAdFailedToShow", argumentsMap("errorCode", error.getCode()));
      }

    };
    this.onPaidEventListener = new OnPaidEventListener() {
      @Override
      public void onPaidEvent(AdValue adValue) {
        Log.w(TAG, "RewardedVideo onPaidEvent: " + adValue.getPrecisionType() + " , " + adValue.getCurrencyCode() + " , " + adValue.getValueMicros());
        Map<String, Object> responseInfo = new HashMap<>();
        if ( adResponseInfo != null ) {
          responseInfo.put("responseId", adResponseInfo.getResponseId());
          responseInfo.put("mediationAdapterClassName", adResponseInfo.getMediationAdapterClassName());
        }
        Map<String, Object> arguments = argumentsMap(
                "precisionType", adValue.getPrecisionType(),
                "currencyCode", adValue.getCurrencyCode(),
                "valueMicros", adValue.getValueMicros(),
                "responseInfo", responseInfo
        );
        RewardedVideoAdWrapper.this.channel.invokeMethod("onPaidEvent", arguments);
      }
    };

    this.onUserEarnedRewardListener = new OnUserEarnedRewardListener() {

      @Override
      public void onUserEarnedReward(RewardItem rewardItem) {
        RewardedVideoAdWrapper.this.channel.invokeMethod(
          "onUserEarnedReward",
          argumentsMap("rewardType", rewardItem.getType(), "rewardAmount", rewardItem.getAmount()));
      }
    };
  }

  Status getStatus() {
    return status;
  }

  public void setUserId(String userId) {
    this.userId = userId;
    buildServerSideVerificationOptions(this.userId, this.customData);
  }

  public void setCustomData(String customData) {
    this.customData = customData;
    buildServerSideVerificationOptions(this.userId, this.customData);
  }

  private void buildServerSideVerificationOptions(String userId, String customData) {
    if ( userId != null || customData != null ) {
      ServerSideVerificationOptions.Builder builder = new ServerSideVerificationOptions.Builder();
      if (userId != null) {
        builder.setUserId(userId);
      }
      if (customData != null) {
        builder.setCustomData(userId);
      }
      rewardedInstance.setServerSideVerificationOptions(builder.build());
    }
  }

  public void load(String adUnitId, String userId, String customData, Map<String, Object> targetingInfo) {
    status = Status.LOADING;
    this.userId = userId;
    this.customData = customData;
    AdRequestBuilderFactory factory = new AdRequestBuilderFactory(targetingInfo);
    RewardedAd.load(activity, adUnitId, factory.createAdRequestBuilder().build(), this.rewardedAdLoadCallback);
  }

  public void show() {
    if (rewardedInstance != null) {
      rewardedInstance.show(this.activity, onUserEarnedRewardListener);
    }
  }

  public void destroy() {
    rewardedInstance = null;
  }

  private Map<String, Object> argumentsMap(Object... args) {
    Map<String, Object> arguments = new HashMap<String, Object>();
    arguments.put("id", id);
    for (int i = 0; i < args.length; i += 2) arguments.put(args[i].toString(), args[i + 1]);
    return arguments;
  }

  static RewardedVideoAdWrapper createRewardedVideo(Integer id, Activity activity, MethodChannel channel) {
    RewardedVideoAdWrapper ad = getAdForId(id);
    return (ad != null) ? (RewardedVideoAdWrapper) ad : new RewardedVideoAdWrapper(id, activity, channel);
  }

  static RewardedVideoAdWrapper getAdForId(Integer id) {
    return allAds.get(id);
  }

  static void disposeAll() {
    for (int i = 0; i < allAds.size(); i++) {
      allAds.valueAt(i).dispose();
    }
    allAds.clear();
  }

  void dispose() {
    rewardedInstance = null;
    allAds.remove(id);
  }
}
